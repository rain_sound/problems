//1. Composite function
const rokket = a => b => c => a*b*c  
//2. Longest string
const rokket = list => list.sort((a,b) => b.length - a.length)[0]
//3. String repetition
const rokket = (word, count) => Array(count + 1).join(word)
//4. Only last names
const rokket = contacts => contacts.map(c => c.lastName)
//5. Unique numbers
const rokket = (first, second) => [...new Set(first.concat(second))]
